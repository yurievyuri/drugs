<?php

    $filterName = 'GRID_DRUGS';
    $gridName = 'GRID_DRUGS';
    $filterHeader = [
        'FILTER_ID' => $filterName,
        'GRID_ID' => $gridName,
        'FILTER' => [
            ['id' => 'DATE', 'name' => 'Дата', 'type' => 'date', 'default' => true],
            [
                'id' => 'USER_ID',
                'name' => 'Ответственный',
                'type' => 'dest_selector',
                'default' => true,
                'params' => [
                    'useNewCallback' => 'Y',
                    //'lazyLoad' => 'Y',
                    //'context' => $propName,
                    'contextCode' => 'U',
                    'enableUsers' => 'Y',
                    'userSearchArea' => 'I', // E
                    'enableSonetgroups' => 'N',
                    'enableDepartments' => 'N',
                    'allowAddSocNetGroup' => 'N',
                    'departmentSelectDisable' => 'Y',
                    'showVacations' => 'Y',
                    'duplicate' => 'N',
                    'enableAll' => 'N',
                    'departmentFlatEnable' => 'N',
                    'useSearch' => 'Y',
                    'multiple' => 'Y',
                    'enableEmpty' => 'N'
                ]
            ],
        ],
        'ENABLE_LIVE_SEARCH' => 'N',
        'ENABLE_LABEL' => 'N',
        'THEME' => 'BORDER'
    ];

    //\Bitrix\UI\Toolbar\Facade\Toolbar::addFilter($filterHeader);
    // Получаем данные для фильтрации.
    //$filterOptions = new \Bitrix\Main\UI\Filter\Options( $filterName );
    //$filterFields = $filterOptions->getFilter();

    $grid_options = new Bitrix\Main\Grid\Options($gridName);
    $sort = $grid_options->GetSorting(['sort' => ['ID' => 'ASC'], 'vars'=>array('by'=>'by', 'order'=>'order')  ]);
    $nav_params = $grid_options->GetNavParams();

    if ( !isset($_REQUEST['FILTER']) && $drugs->getEditMode() === 'Y' )
    {
        $_REQUEST['FILTER'] = 'on';
    }
    $total = $drugs->getRowCountTotal();
    $nav = new Bitrix\Main\UI\PageNavigation($gridName);
    //$nav->allowAllRecords(true)
    $nav->setRecordCount( $total )
        ->setPageSize($nav_params['nPageSize'])
        ->initFromUri();

    $nav_params['iNumPage'] = $nav->getCurrentPage();
    //$nav_params['nTopCount'] = $nav->getLimit();
    $nav_params['nPageSize'] = $nav->getPageSize();

    $APPLICATION->IncludeComponent(
        'bitrix:main.ui.grid',
        '',
        [
            'GRID_ID' => $gridName,
            'NAV_OBJECT' => $nav,
            'ROWS' => $drugs->getRows(false, $nav_params),
            'COLUMNS' => [
                ['id' => 'ID', 'name' => 'ID', 'default' => true ],
                ['id' => 'DRUG', 'name' => 'Препарат', 'default' => true],
                ['id' => 'ZONE', 'name' => 'Зона', 'default' => true],
                ['id' => 'FX', 'name' => 'Эффект применения', 'default' => true],
                ['id' => 'SEGMENT', 'name' => 'Сегмент', 'default' => true],
                ['id' => 'ITERATION', 'name' => 'Итерация', 'default' => true],
                ['id' => 'GENDER', 'name' => 'Пол', 'default' => true],
            ],
            'AJAX_MODE' => 'Y',
            'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', $gridName),
            'PAGE_SIZES' => [
                ['NAME' => '5', 'VALUE' => '5'],
                ['NAME' => '10', 'VALUE' => '10'],
                ['NAME' => '25', 'VALUE' => '25'],
                ['NAME' => '50', 'VALUE' => '50'],
                ['NAME' => '100', 'VALUE' => '100'],
            ],
            'SHOW_ROW_CHECKBOXES'       => false,
            'AJAX_OPTION_JUMP'          => 'N',
            'SHOW_CHECK_ALL_CHECKBOXES' => false,
            'SHOW_ROW_ACTIONS_MENU'     => true,
            'SHOW_GRID_SETTINGS_MENU'   => true,
            'SHOW_NAVIGATION_PANEL'     => true,
            'SHOW_PAGINATION'           => true,
            'SHOW_SELECTED_COUNTER'     => false,
            'SHOW_TOTAL_COUNTER'        => true,
            'SHOW_PAGESIZE'             => true,
            'SHOW_ACTION_PANEL'         => false,
            'ALLOW_COLUMNS_SORT'        => true,
            'ALLOW_COLUMNS_RESIZE'      => true,
            'ALLOW_HORIZONTAL_SCROLL'   => true,
            'ALLOW_SORT'                => true,
            'ALLOW_PIN_HEADER'          => true,
            'AJAX_OPTION_HISTORY'       => 'Y',
            'TOTAL_ROWS_COUNT'          => $total,
            'CURRENT_PAGE' => $nav->getCurrentPage()
        ]
    );