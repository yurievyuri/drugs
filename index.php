<?php
    set_time_limit(30);
    require( $_SERVER[ 'DOCUMENT_ROOT' ]. '/bitrix/header.php' );
    //CJSCore::Init(array("jquery"));
    require ( 'class.php' );

    $APPLICATION->SetTitle('Формирование данных о препаратах');

    \Bitrix\Main\Page\Asset::getInstance()->addCss( '/local/assets/semantic/semantic.css' );
    \Bitrix\Main\Page\Asset::getInstance()->addJs( '/local/assets/jquery/jquery-3.5.0.js' );
    \Bitrix\Main\Page\Asset::getInstance()->addJs( '/report/drugs/script.js' );

    $drugs = new \Dev\Utils\Drugs();

    ?>

    <style>
        .workarea-content-paddings {
            overflow-x: visible!important;
        }
    </style>

    <div class="ui basic segment main"  style="margin-top: 0;
    padding-top: 20px;">

        <div class="messageField"></div>

        <h2 class="ui main header"></h2>

        <form class="ui form main" type="POST" action="ajax.php">

            <input type="hidden" name="ID">
            <input type="hidden" readonly="true" name="apply_filter" value="Y">
            <input type="hidden" readonly="true" name="clear_nav" value="Y">

            <h4 class="ui dividing header">Настройки препарата</h4>

            <div class="fields">
                <div class="four wide field">
                    <label>Зона использования:</label>
                    <div class="ui selection search dropdown zoneField">
                        <input type="hidden" name="ZONE">
                        <i class="dropdown icon"></i>
                        <div class="default text">Выбрать зону</div>
                        <div class="menu"></div>
                    </div>
                </div>

                <div class="twelve wide field">
                    <label>Достигаемый эффект:</label>
                    <div class="ui multiple selection search dropdown fxField disabled">
                        <input type="hidden" name="FX">
                        <i class="dropdown icon"></i>
                        <div class="default text">Выбрать эффект</div>
                        <div class="menu"></div>
                    </div>
                </div>

            </div>


            <div class="fields" style="margin: 22px 0 15px 0px;" >

                <div class="field" style="background: rgba(200,100,0,0.1);border-radius: 5px;
    margin-right: 10px;">
                    <label>Пол:</label>
                    <div class="fields inline">
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="GENDER[M]" value="M">
                                <label>Мужской</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="GENDER[F]" value="F">
                                <label>Женский</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="field" style="background: rgba(200,100,200,0.1);border-radius: 5px;
    margin-right: 10px;">
                    <label>Итерация:</label>
                    <div class="fields inline">
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="ITERATION[F]" value="F">
                                <label>Первичная</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="ITERATION[S]" value="S">
                                <label>Повторная</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="field" style="background: rgba(0,100,200,0.1);border-radius: 5px;
    margin-right: 5px;">
                    <label>Сегмент:</label>
                    <div class="fields inline">
                        <div class="field">
                            <div class="ui checkbox segmentField">
                                <input type="checkbox" name="SEGMENT[E]" value="E">
                                <label>Эконом</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox segmentField">
                                <input type="checkbox" name="SEGMENT[P]" value="P">
                                <label>Премиум</label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="fields">
                <div class="sixteen wide field">
                    <label>Препарат:</label>

                    <div class="ui selection search dropdown drugField">
                        <input type="hidden" name="DRUG">
                        <i class="dropdown icon"></i>
                        <div class="default text">Выбрать препарат</div>
                        <div class="menu"></div>
                    </div>

                </div>
            </div>

            <div class="field">

                <button class="ui button" <?php if( $drugs->getEditMode() !== 'Y') {
                    echo 'disabled="disabled" ';
                } ?> type="submit">Сохранить</button>
<!--                <button class="ui button" type="button">Обновить</button>-->

                <button class="ui button" type="reset">Очистить</button>

            </div>

            <div class="field">
                <div class="fields inline">
                    <div class="field copyField disabled">
                        <div class="ui checkbox">
                            <input type="checkbox" name="COPY">
                            <label>копировать</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui checkbox filterField noReset

                        <?php if( $drugs->getEditMode() === 'Y') {
                            echo ' checked '; //="checked" value="on"
                        } ?>

                            ">
                            <input type="checkbox" name="FILTER"  />
                            <label>режим редакции</label>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>


    <script src="/local/assets/semantic/semantic.js"/>

<?php
    require ('grid.php');
    require ($_SERVER[ 'DOCUMENT_ROOT' ]. '/bitrix/footer.php' );