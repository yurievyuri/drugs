document.addEventListener("DOMContentLoaded", Drugs );
let cache_list = {
    redaction : false,
    result : {
        drugs_list: false,
        drugs_list_used : false,
        zones_list: false,
        fxs_list: false
    }
};
function Drugs( pref = {} )
{
    console.clear();
    let
    gridObject = BX.Main.gridManager.getById('GRID_DRUGS'),
    setup = {
        conf: false,
        dropdown: false,
        properties: {
            checkbox : [ 'GENDER', 'ITERATION', 'SEGMENT'],
            dropdown: [ 'ZONE', 'DRUG', 'FX' ]
        },

        init: () =>
        {
          if ( !setup.conf )
          {
              setup.conf = {
                    segment : document.querySelector('.ui.segment.main'),
                    form: document.querySelector('.ui.form.main'),
                    id: document.querySelector('.ui.form.main').querySelector('[name="ID"]'),
                    header : document.querySelector('.ui.main.header'),
                    copy: document.querySelector('.copyField'),
                    filter: document.querySelector('[name="FILTER"]'),
                    filterField : document.querySelector('.filterField'),
                    messageField: document.querySelector('.messageField'),
                    path : '/report/drugs/',
                    ajax_file : 'ajax.php'
              };
          }
          if ( !setup.dropdown )
          {
              setup.dropdown  = {
                  zone : document.querySelector('.zoneField'),
                  fx : document.querySelector('.fxField'),
                  drug : document.querySelector('.drugField'),
                  ui : {
                      zone : false,
                      fx : false,
                      drug : false
                  }
              }
          }
        },
        semantic : () =>
        {
            if ( cache_list.result.zones_list )
            {
                setup.dropdown.ui.zone = $(setup.dropdown.zone).dropdown(
                    {
                        clearable: true,
                        forceSelection: false,
                        fullTextSearch : true,
                        values: cache_list.result.zones_list,
                        onChange: function( id )
                        {
                            if ( id === false ) return;
                            setup.reset.checkbox();
                            setup.update.fx(id);
                        }
                    }
                );
            }

            if ( cache_list.result.drugs_list || cache_list.result.drugs_list_used )
            {
                setup.update.drugs();
            }

            setup.dropdown.ui.fx = $(setup.dropdown.fx).dropdown(
                {
                    clearable: true,
                    forceSelection: false,
                    fullTextSearch: true,
                    allowAdditions: true
                }
            );

            /*$( '.segmentField' ).checkbox({

                onChecked: () => {

                    if ( $(setup.conf.filterField).checkbox('is checked') )  return;

                    $(setup.dropdown.drug)
                        .dropdown('clear');
                }
            });*/

            let submitButton = setup.conf.form.querySelector('[type="submit"]');
            $(setup.conf.filterField).checkbox({

                onUnchecked: () => {

                    if ( !submitButton ) return false;

                    if ( !submitButton.getAttribute('disabled'))
                        submitButton.setAttribute('disabled', 'disabled');

                    cache_list.redaction = false;

                    setup.update.drugs();

                    setup.load('edit_mode');

                    /*if ( setup.dropdown.drug.classList.contains('multiple') )
                        setup.dropdown.drug.classList.remove('multiple');*/
                    /*$(setup.dropdown.drug)
                        .dropdown({
                        maxSelections: 10
                    });*/
                },
                onChecked: () => {

                    if ( !submitButton ) return false;

                    if ( submitButton.getAttribute('disabled'))
                        submitButton.removeAttribute('disabled');

                    /*if ( !setup.dropdown.drug.classList.contains('multiple') )
                        setup.dropdown.drug.classList.add('multiple');*/
                    /*$(setup.dropdown.drug)
                        .dropdown({
                        maxSelections: 1
                    }).dropdown('clear');*/

                    cache_list.redaction = true;

                    setup.update.drugs();

                    setup.load('edit_mode');

                    //setup.conf.filter.value = 'on';
                    //setup.conf.filter.setAttribute('readonly', 'readonly');
                    //setup.conf.filterField.addClass('disabled');
                }

            });

            // события
            setup.conf.form.addEventListener('submit', function(event)
            {
                event.preventDefault();
                event.stopPropagation();
                setup.load('save');

            });

            setup.conf.form.addEventListener('reset', function( event )
            {
                event.preventDefault();
                event.stopPropagation();
                setup.reset.form();
            });

            setup.conf.form.addEventListener('change', function(event, x)
            {
                event.preventDefault();
                event.stopPropagation();

                if ( event.target.name === 'FILTER' ) return false;
                if ( event.target.name === 'COPY' ) return false;

                if ( cache_list.redaction === true ) return false;
                if ( $(setup.conf.filterField).checkbox('is checked') ) return false;

                grid.update( 'event listener form change' );

            });
        },

        update : {

            header : (text = '') => {

                setup.conf.header.innerHTML = text;
            },
            fx: (id) =>
            {
                setup.dropdown.ui.fx.dropdown('clear');

                if ( setup.dropdown.fx.classList.contains('error') )
                {
                    setup.dropdown.fx.classList.remove('error');
                }
                if ( setup.dropdown.fx.classList.contains('disabled') )
                {
                    setup.dropdown.fx.classList.remove('disabled');
                }
                if ( !id || !cache_list.result.fxs_list[id] )
                {
                    setup.dropdown.fx.classList.add('disabled');
                    setup.dropdown.fx.classList.add('error');
                    return false;
                }

                setup.dropdown.fx.classList.add('loading');

                $(setup.dropdown.fx).dropdown({

                    values: cache_list.result.fxs_list[id]

                });

                setTimeout(

                    () => setup.dropdown.fx.classList.remove('loading'), 500
                )
            },

            copy : () =>
            {
                if ( setup.conf.copy.classList.contains('disabled') )
                    setup.conf.copy.classList.remove('disabled');
            },

            drugs : () =>
            {
                setup.dropdown.drug.classList.add('loading');
                setup.dropdown.ui.drug = $( setup.dropdown.drug )
                    .dropdown(
                        {
                            ignoreCase: true,
                            //searchOnFocus: false,
                            //selectFirstResult: false,
                            forceSelection: false,
                            clearable: true,
                            fullTextSearch: true,
                            values: ( cache_list.redaction )
                                    ? cache_list.result.drugs_list
                                    : cache_list.result.drugs_list_used
                            ,
                            onSelect : ( data ) => {
                                if ( data === false ) return false;
                            }

                    }).dropdown('refresh')
                ;

                setTimeout(
                    () => setup.dropdown.drug.classList.remove('loading'), 500
                );
            }

        },

        reset : {

            checkbox : () =>
            {
                $('.checkbox').not('.noReset')
                    .checkbox('uncheck')
                    .checkbox('set unchecked');
            },

            form : () =>
            {
                //setup.conf.form.reset();
                setup.error.hide();
                setup.conf.id.value = '';
                setup.reset.dropdown();
                setup.reset.checkbox();
                setup.update.header();
                setup.conf.segment.classList.remove('red');
                setup.reset.copy();
                grid.update( 'form_reset' );
            },

            dropdown : () =>
            {
                $('.dropdown').dropdown('clear');
                setup.dropdown.fx.classList.add('disabled');
            },

            copy : () =>
            {
                if ( !setup.conf.copy.classList.contains('disabled') )
                    setup.conf.copy.classList.add('disabled');
            }
        },

        alert : {
            show: ( text ) =>
            {


            },

            hide: () =>
            {

            }
        },
        error : {

            show: ( text = 'Произошла неизвестная ошибка...' ) =>
            {
                setup.conf.messageField.innerHTML = '<div class="ui red message">'+text+'</div>';
            },
            hide: () =>
            {
                setup.conf.messageField.innerHTML = '';
            }
        },

        load: ( loadCommand = '') =>
        {
            loading( setup.conf.segment, 'active' );

            setup.error.hide();

            BX.ajax({
                'url': setup.conf.path + setup.conf.ajax_file + '?' + loadCommand,
                'method': 'post',
                'data': serialize(setup.conf.form),
                'dataType': 'json', //html|json|script – what type of data is expected in the response
                //'timeout': 300,
                'async': true,
                //'processData': true,
                //'scriptsRunFirst': true,
                //'emulateOnload': true,
                //'start': true, //|false – whether to send the request immediately or it will be launched manually
                'cache': false, // – if false, a random chunk will be added to the URL parameter to avoid browser caching
                onsuccess: function (data)
                {
                    loading(setup.conf.segment, 'remove');

                    setup.result = data;

                    if ( data && data.redaction === 'Y' )
                        cache_list.redaction = true;
                    else if ( data && data.redaction === 'N' )
                        cache_list.redaction = false;

                    if ( data.action && data.action.error )
                    {
                        setup.error.show( data.action.error );
                        return false;
                    }

                    if( !cache_list.result.drugs_list )
                    {
                        cache_list.result = data;
                    }

                    /*if ( data.success && data.success === true)
                    {
                    }
                    if ( data && data.action && data.action['id'] > 0 )
                    {
                        setup.conf.id.value = data.action['id'];
                    }*/
                    //else if( data.error && data.error === true )

                    if ( loadCommand === 'init' )
                    {
                        setup.semantic();
                    }

                    //if ( loadCommand === 'save' || loadCommand === 'edit_mode' )
                    //    grid.update( 'load' );

                    if ( loadCommand !== 'save' )
                        setup.reset.form();
                },
                onerror: function( data, x )
                {
                    setup.result = data;
                    setup.error.show( x );
                    console.error( x );
                    loading( setup.conf.segment, 'remove' );
                },
                onfailure: function(data, x)
                {
                    setup.result = data;
                    setup.error.show( x );
                    console.error( x );
                    loading( setup.conf.segment, 'remove' );
                }
            });

            return true;
        }
    };

    let grid = {

        update : ( from = '' ) =>
        {
            console.log( 'from function: ' + from );

            if (gridObject.hasOwnProperty('instance'))
            {
                //let postForm = serialize(setup.conf.form);
                let
                    postForm = $(setup.conf.form).serializeArray(),
                    postObject = {}
                ;

                Array.from(postForm).map(( item )=> {

                    if ( !item.value ) return;
                    postObject[ item.name ] = item.value;

                });

                gridObject.instance.reloadTable('POST', postObject ); // reloadParams { apply_filter: 'Y', clear_nav: 'Y' }
            }
        },
        edit: function ( pref )
        {
            setup.init();
            // id
            if ( pref.data['ID'] )
            {
                setup.conf.id.value = pref.data['ID'];
            }
            // dropdowns
            if ( pref.data[ 'PROPERTY_ZONE_VALUE' ] )
            {
                $(setup.dropdown.zone)
                    .dropdown('clear')
                    .dropdown('set selected', pref.data[ 'PROPERTY_ZONE_VALUE' ] )
                    .change();
            }
            if ( pref.data[ 'PROPERTY_DRUG_VALUE' ] )
            {
                $(setup.dropdown.drug)
                    .dropdown('clear')
                    .dropdown('set selected', pref.data[ 'PROPERTY_DRUG_VALUE' ] )
                    .change();
            }
            if ( pref.data[ 'PROPERTY_FX_VALUE' ] )
            {
                $(setup.dropdown.fx)
                    .dropdown('clear')
                    .dropdown('set selected', pref.data[ 'PROPERTY_FX_VALUE' ] )
                    .change();
            }
            // checkbox
            setup.reset.checkbox();
            setup.properties.checkbox.forEach( ( prop) =>
            {
                if ( !pref.data['PROPERTY_'+prop+'_VALUE'] ) return;

                pref.data['PROPERTY_'+prop+'_VALUE'].forEach(function( index )
                {
                    let checkbox = setup.conf.form.querySelector('[name="'+prop+'['+ index +']"]');
                    if ( !checkbox ) return;
                    checkbox.click();
                });
            });

            setup.update.header( '#' + pref.data['ID'] + ' - ' + pref.data['NAME'] );
            setup.conf.segment.classList.add('red');
            setup.update.copy();
        },
        remove : function ( pref )
        {
            setup.init();
            if ( !pref.id ) return false;
            if ( !setup.conf.id ) return false;
            if ( cache_list && cache_list.delete === pref.id ) return false;

            cache_list['remove'] = pref.id;
            setup.conf.id.value = pref.id;
            setup.load('remove');
        }
    };

    if (pref.type === 'DOMContentLoaded')
    {
        setup.init();
        setup.load('init');
    }

    if ( pref.action === 'edit')
        grid.edit(pref);
    if ( pref.action === 'remove')
        grid.remove(pref);

}

function loading( object, command = 'remove' )
{
    if ( command !== 'remove' )
        object.classList.add('loading');
    else
        object.classList.remove('loading');
}

/*!
 * Serialize all form data into a query string
 * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {Node}   form The form to serialize
 * @return {String}      The serialized form data
 */
var serialize = function (form)
{
    // Setup our serialized data
    var serialized = [];

    // Loop through each field in the form
    for (var i = 0; i < form.elements.length; i++) {

        var field = form.elements[i];

        // Don't serialize fields without a name, submits, buttons, file and reset inputs, and disabled fields
        if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;

        // If a multi-select, get all selections
        if (field.type === 'select-multiple') {
            for (var n = 0; n < field.options.length; n++) {
                if (!field.options[n].selected) continue;
                serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[n].value));
            }
        }

        // Convert field data to a query string
        else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
            serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value));
        }
    }

    return serialized.join('&');

};