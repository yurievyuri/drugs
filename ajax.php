<?php
    define('NO_KEEP_STATISTIC', 'Y');
    define('NO_AGENT_STATISTIC','Y');
    define('NO_AGENT_CHECK', true);
    define('STOP_STATISTICS', true);
    define('PUBLIC_AJAX_MODE', true);
    define('DisableEventsCheck', true);
    require_once($_SERVER[ 'DOCUMENT_ROOT' ]. '/bitrix/modules/main/include/prolog_before.php' );
    \Bitrix\Main\Loader::includeModule('main');
    require ( 'class.php' );
    $drugs = new \Dev\Utils\Drugs();

    if ( isset($_REQUEST['init']) )
    {
        $result['drugs_list'] = $drugs->getDrugsListEx();
        $result['drugs_list_used'] = $drugs->getUsedDrugsList();
        $result['zones_list'] = $drugs->getZonesListEx();
        $result['fxs_list'] = $drugs->getFxsListEx();
        $result['redaction'] = $drugs->getEditMode();
    }

    if ( isset($_REQUEST['save']) )
    {
        $result['request'] = $_REQUEST;
        $result['action'] = $drugs->saveData();
    }

    if ( isset($_REQUEST['remove']) && $_REQUEST['ID'] > 0)
    {
        $result[ 'action' ] = $drugs->removeData();
    }

    if ( isset($_REQUEST['edit_mode']) )
    {
        $result['action'] = $drugs->setEditMode();
    }

    $result['success'] = true;

    $GLOBALS['APPLICATION']->RestartBuffer();
    Header('Content-Type: application/json');
    echo json_encode( $result );
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
    die();