<?php
namespace Dev\Utils;
use Bitrix\Iblock\SectionTable;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Config\Option;

/**
 * @property array zonesList
 * @property array drugsList
 * @property  array fxsList
 * @property  array property_list
 * @property  array drug
 * @property  array zone
 * @property  array fx
 * @property array property_name_list
 * @property array property_required_list
 * @property bool|int rowCountTotal
 * @property array rowResult
 * @property array usedDrugsList
 */
class Drugs
{
    public const prod = 'fillersbotox.ru';
    public const setup = [
        'dev' => [
            'IBLOCK_ID_DRUGS' => 27,
            'IBLOCK_ID_ZONES' => 134,
            'IBLOCK_ID_DRUGS_MATRIX' => 136,
            'TTL' => 0,
            'LIMIT' => 3000
        ],
        'prod' => [
            'IBLOCK_ID_DRUGS' => 27,
            'IBLOCK_ID_ZONES' => 146,
            'IBLOCK_ID_DRUGS_MATRIX' => 147,
            'TTL' => 86000,
            'LIMIT' => 3000
        ]
    ];

    public const multiple = ['FX'];
    public const checkbox = [ 'GENDER', 'ITERATION', 'SEGMENT' ];
    public const gender = [
        'M' => 'мужской',
        'F' => 'женский'
    ];
    public const segment = [
        'E' => 'эконом',
        'P' => 'премиум'
    ];
    public const iteration = [
        'F' => 'первичная',
        'S' => 'повторная'
    ];
    public function __construct()
    {
    }

    public function getUsedDrugsList() : array
    {
        $array = [];

        if ( $this->usedDrugsList )
        {
            return $this->usedDrugsList;
        }

        $this->usedDrugsList = []; // = [ 'value' => false, 'name' => '...', 'text' => '...' ];

        $filter = $this->getDefaultFilter(true);
        $db = \CIBlockElement::GetList(
            false,
            $filter,
            false,
            false,
            array_merge(['ID', 'PROPERTY_DRUG', 'PROPERTY_DRUG.NAME'])
        );

        while ( $list = $db->Fetch() )
        {
            $array[ $list['PROPERTY_DRUG_VALUE'] ] = $list[ 'PROPERTY_DRUG_NAME' ];
        }

        asort( $array);

        foreach ( $array as $id => $item )
        {
            $this->usedDrugsList[] = [ 'value' => $id, 'name' => trim($item), 'text' => trim($item) ];
        }

        return $this->usedDrugsList;
    }

    /**
     * Получение списка препаратов
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getDrugsListEx() : array
    {
        //$this->drugsList[] = [ 'value' => false, 'name' => '...', 'text' => '...' ];

        $db = ElementTable::getList([
            'order' => ['NAME' => 'ASC'],
            'filter' => [
                'IBLOCK_ID' => $this->setup('IBLOCK_ID_DRUGS'),
                'ACTIVE' => 'Y'
            ],
            'cache' => [
                'ttl' => $this->setup('TTL')
            ],
            'select' => ['ID', 'NAME'],
            'limit' => $this->setup('LIMIT')
        ]);

        while ( $list = $db->Fetch() )
        {
            $this->drug[$list['ID']] = trim($list['NAME']);
            $this->drugsList[] = [ 'value' => $list['ID'], 'name' => trim($list['NAME']), 'text' => trim($list['NAME']) ];
        }

        return $this->drugsList;
    }

    /**
     * Зона воздействия процедуры
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getZonesListEx() : array
    {
        $db = SectionTable::getList([
            'order' => ['ID' => 'ASC'],
            'filter' => [
                'IBLOCK_ID' => $this->setup('IBLOCK_ID_ZONES'),
                'ACTIVE' => 'Y'
            ],
            'select' => ['ID', 'NAME'],
            'cache' => [
                'ttl' => $this->setup('TTL')
            ],
            'limit' => $this->setup('LIMIT')
        ]);

        while ( $list = $db->Fetch() ) {

            $this->zone[$list['ID']] = trim($list['NAME']);
            $this->zonesList[] = [ 'value' => $list['ID'], 'name' => $list['NAME'], 'text' => $list['NAME'] ];
        }

        return $this->zonesList;
    }

    /***
     * Получаем эффект от процедур
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getFxsListEx() : array
    {
        if ( !$this->zonesList ){
            $this->getZonesListEx();
        }

        $db = ElementTable::getList([
            'order' => ['NAME' => 'ASC'],
            'filter' => [
                'IBLOCK_ID' => $this->setup('IBLOCK_ID_ZONES'),
                'ACTIVE' => 'Y'
            ],
            'select' => ['*'],
            'cache' => [
                'ttl' => $this->setup('TTL')
            ],
            'limit' => $this->setup('LIMIT')
        ]);

        while ( $list = $db->Fetch() )
        {
            $this->fx[$list['ID']] = trim($list['NAME']);
            $this->fxsList[ $list['IBLOCK_SECTION_ID'] ][] = [ 'value' => $list['ID'], 'name' => trim($list['NAME']), 'text' => trim($list['NAME']) ];
        }

        return $this->fxsList;
    }

    /**
     *
     */
    public function saveData() : array
    {
        if ( !$_REQUEST )
        {
            return [ 'error' => 'Нет данных для сохранения'];
        }
        $result = [];

        $array = Array(
          'IBLOCK_ID'           =>  $this->setup('IBLOCK_ID_DRUGS_MATRIX'),
          'ACTIVE'              =>  'Y'
        );

        $this->prepareRequest( $array);

        $element = new \CIBlockElement();
        if ( $_REQUEST['ID'] && !isset($_REQUEST['COPY']) )
        {
            $result['id'] = $element->Update( (int)$_REQUEST['ID'], $array );
        }
        else
        {
            $result['id'] = $element->Add( $array );
        }

        if ( $element->LAST_ERROR ) {
            $result[ 'error' ] = $element->LAST_ERROR;
        }

        return $result;
    }

    /**
     * @return array|bool
     */
    public function removeData()
    {
        if ( !$_REQUEST['ID'] )
        {
            return [ 'error' => 'Не указан ID элемента для удаления'];

        }
        $result = [];

        $element = new \CIBlockElement();
        $result = $element->Delete( (int)$_REQUEST['ID']);

        if ( $element->LAST_ERROR ) {
            $result[ 'error' ] = $element->LAST_ERROR;
        }

        return $result;
    }


    /**
     * @param $array
     * @return array
     */
    public function prepareRequest( &$array ) : array
    {
        if( !$this->property_required_list )
        {
            $this->getPropertyList();
        }

        $array['NAME'] = 'noname';

        if ( $_REQUEST['DRUG'] )
        {
            $array['NAME'] = \CIBlockElement::GetByID(  $_REQUEST['DRUG'])->Fetch()['NAME'] ? : 'Наименование не удалось получить';
        }

        foreach ( self::multiple as $prop )
        {
            if ( !$_REQUEST[$prop] ) {
                continue;
            }
            if ( !is_string( $_REQUEST[$prop]) ) {
                continue;
            }
            $_REQUEST[ $prop ] = explode(',', $_REQUEST[ $prop ] );
        }

        /*foreach ( self::checkbox as $prop )
        {
            if( !$_REQUEST[$prop] ) {
                continue;
            }
            $_REQUEST[ $prop ] = array_keys( $_REQUEST[ $prop ] );
        }*/

        $array['PROPERTY_VALUES'] = $_REQUEST;

        foreach ( $this->property_required_list  as $property )
        {
            if ( isset($array['PROPERTY_VALUES'][$property]) ) continue;

            $array['PROPERTY_VALUES'][$property] = false;
        }

        return $array;
    }

    /**
     * @param $key
     * @param string $mode
     * @return mixed
     */
    public function setup( $key, $mode = 'dev' )
    {
        if ( $_SERVER['SERVER_NAME'] === self::prod )
        {
            $mode = 'prod';
        }

        if ( self::setup[$mode][$key] )
        {
            return self::setup[ $mode ][ $key ];
        }

        if ( self::setup[ $key] )
        {
            return self::setup[ $key ];
        }
    }


    /**
     * @return array
     */
    public function getPropertyList() : array
    {
        $db = \CIBlockProperty::GetList(
            [],
            Array( 'ACTIVE' => 'Y', 'IBLOCK_ID' => $this->setup('IBLOCK_ID_DRUGS_MATRIX') )
        );
        while ( $list = $db->Fetch() )
        {
            $this->property_list[] = 'PROPERTY_' . $list[ 'CODE' ];
            $this->property_name_list[] = $list[ 'CODE' ];
            if ( $list['IS_REQUIRED'] === 'Y' )
            {
                $this->property_required_list[] = $list[ 'CODE' ];
            }
        }

        return $this->property_list;
    }

    /**
     * @param $filter
     * @return array
     */
    public function matchProperties( &$filter ) : array
    {
        if ( !$this->property_name_list )
        {
            $this->getPropertyList();
        }

        if ( !$_REQUEST ) {
            return $filter;
        }

        foreach ( $_REQUEST as $key => $value )
        {
            if ( in_array($key, $this->property_name_list, true ))
            {
                if ( in_array( $key, self::multiple, true ) )
                {
                    $value = explode(',', $value);
                }
                if ( in_array( $key, self::checkbox, true ) )
                {
                    $value = array_keys( $value);
                }

                $filter['PROPERTY_'.$key] = $value;
            }
        }

        return $filter;
    }

    /**
     * @param array $sort
     * @param bool $navParams
     * @return array|void
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getRows( $sort = [   'DATE_CREATE' => 'DESC'   ], $navParams = false )
    {
        $result = [];

        if ( !$this->property_list )
        {
            $this->getPropertyList();
        }

        $filter = $this->getDefaultFilter();

        $db = \CIBlockElement::GetList(
            $sort,
            $filter,
            false,
            $_REQUEST['FILTER'] === 'on' ? $navParams : false,
            array_merge(['ID', 'NAME', 'DATE_MODIFY'],$this->property_list)
        );

        $result = $_REQUEST['FILTER'] !== 'on' ? $this->groupRow($db, $navParams) : $this->simpleRow($db);

        return $result;
    }


    /**
     * @param $db
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function simpleRow( $db ) : array
    {
        if ( $this->rowResult )
        {
            return $this->rowResult;
        }

        $result = [];

        while ( $list = $db->Fetch() )
        {
            $this->renderPrepare( $list);

            $result[ $list['ID'] ] = [
                'data' => $list,
                'actions' => [
                    [
                        'text'    => 'Редактировать',
                        'onclick' => 'Drugs({ action: "edit", id: '.$list['ID'].', data: '. json_encode($list). '});',
                    ],
                    [
                        'text'    => 'Удалить',
                        'onclick' => 'Drugs({ action: "remove", id: '.$list['ID'].'});'
                    ]
                ],
            ];
        }

        return $this->rowResult = $result;
    }

    /**
     * @param $db
     * @param array $navParams
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function groupRow( $db, $navParams = [] ) : array
    {
        /*if ( $this->rowResult )
        {
            $this->rowResult = $this->preparePagination( $this->rowResult, $navParams);
            return $this->rowResult;
        }*/

        $result = [];
        $return = [];
        if ( !$this->property_list )
        {
            $this->getPropertyList();
        }

        while ( $list = $db->Fetch() )
        {
            $result[ $list['PROPERTY_DRUG_VALUE'] ]['NAME'] = $list['NAME'];
            $result[ $list['PROPERTY_DRUG_VALUE'] ]['ID'] = $list['PROPERTY_DRUG_VALUE'];
            $result[ $list['PROPERTY_DRUG_VALUE'] ]['PROPERTY_DRUG_VALUE'] = $list['PROPERTY_DRUG_VALUE'];

            foreach ( $this->property_list as $prop )
            {
                if ( $prop !== 'PROPERTY_DRUG' && !is_array($list[ $prop . '_VALUE' ]) )
                {
                    $list[ $prop . '_VALUE' ] = [ $list[ $prop . '_VALUE' ] ];
                }
                foreach ( $list[ $prop.'_VALUE' ] as $value )
                {
                    if ( in_array($value, $result[ $list['PROPERTY_DRUG_VALUE'] ][ $prop . '_VALUE' ], true ))
                    {
                        continue;
                    }

                    $result[ $list['PROPERTY_DRUG_VALUE'] ][ $prop . '_VALUE' ][] = $value;
                }
            }
        }

        foreach ($result as $list )
        {
            $this->renderPrepare( $list);

            $return[ $list['ID'] ] = [ 'data' => $list ];
        }

        $this->rowCountTotal = count($return);

        return $this->rowResult = $this->preparePagination($return, $navParams);
    }

    /**
     * @param $array
     * @param array $navParams
     * @return array
     */
    public function preparePagination( $array, $navParams = [] ) : array
    {
        if ( !$navParams )
        {
            return $array;
        }
        $result = [];
        $count = 1;
        $page = 1;

        foreach ( $array as $key => $value )
        {
            if ( $count <= $navParams['nPageSize'] )
            {
                $result[$page][$key] = $value;
                $count++;
            }

            if ( $count > $navParams['nPageSize'] )
            {
                $count = 1;
                $page++;
            }
        }
        if ( $result[ $navParams['iNumPage'] ]) {
            return $result[ $navParams[ 'iNumPage' ] ];
        }

        return $result;
    }

    /**
     * @param bool $noProp
     * @return array
     */
    public function getDefaultFilter( $noProp = false ) : array
    {
        $filter = [
            'IBLOCK_ID'         =>  $this->setup('IBLOCK_ID_DRUGS_MATRIX'),
            'ACTIVE'            =>  'Y',
            'CHECK_PERMISSIONS' =>  'N'
        ];

        if( $_REQUEST['FILTER'] !== 'on' && $noProp === false )
        {
            $this->matchProperties( $filter );
        }

        return $filter;
    }


    /**
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getRowCountTotal() : int
    {
        if ( !$this->property_list )
        {
            $this->getPropertyList();
        }

        if ( $_REQUEST['FILTER'] === 'on' ) {
            $this->rowCountTotal = \CIBlockElement::GetList( false, $this->getDefaultFilter() )->SelectedRowsCount();
        }
        else {
            $this->getRows();
        }

        return $this->rowCountTotal ? : 0;
    }

    /**
     * Рендеринг строк в цикле
     * @param $list
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function renderPrepare(&$list) : void
    {
        if ( $list['PROPERTY_FX_VALUE'] )
            $this->render('fx', $list);

        if ( $list['PROPERTY_DRUG_VALUE'] )
            $this->render('drug', $list);

        if ( $list['PROPERTY_ZONE_VALUE'] )
            $this->render('zone', $list);

        if ( $list['PROPERTY_GENDER_VALUE'] )
            $this->render('gender', $list);

        if ( $list['PROPERTY_ITERATION_VALUE'] )
            $this->render('iteration', $list);

        if ( $list['PROPERTY_SEGMENT_VALUE'] )
            $this->render('segment', $list);
    }

    /**
     * @param $type
     * @param $list
     * @return string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function render( $type, &$list ) : string
    {
        $values = $list['PROPERTY_'.strtoupper( $type).'_VALUE'];

        if ( !$this->{$type} && $type === 'fx' ) {
            $this->getFxsListEx();
        }

        if ( !$this->{$type} && $type === 'drug' ) {
            $this->getDrugsListEx();
        }

        if ( !$this->{$type} && $type === 'zone' ) {
            $this->getZonesListEx();
        }

        if ( !$this->{$type} && $type === 'gender' ) {
            $this->{$type} = self::gender;
        }
        if ( !$this->{$type} && $type === 'iteration' ) {
            $this->{$type} = self::iteration;
        }

        if ( !$this->{$type} && $type === 'segment' ) {
            $this->{$type} = self::segment;
        }

        if ( !$this->{$type} ) {
            return $this->htmlView(false,$values, $list);
        }

        if ( !is_array( $values)) {
            return $list[ strtoupper( $type) ] = $this->htmlView( $type,$this->{$type}[ $values ], $list);
        }

        $result = [];

        foreach ( $values as $value )
        {
            $result[] = $this->htmlView( $type, $this->{$type}[$value], $list);
        }

        return $list[ strtoupper( $type) ] = implode('', $result );
    }

    public function htmlView( $type = false, $string, $list ) : string
    {
        if ( $type === 'drug' )
            $string = '<a title="открыть детальное описание товара" target="_blank" href="/crm/product/show/'.$list['ID'].'/">'. $string.'</a>';

        if ( $type === 'segment' && $string === 'премиум' )
            $string = '<span style="background: rgba(0,100,200,0.1)">'.$string.'</span>';

        if ( $type === 'iteration' && $string === 'первичная' )
            $string = '<span style="background: rgba(200,100,200,0.1)">'.$string.'</span>';

        if ( $type === 'gender' && $string === 'женский' )
            $string = '<span style="background: rgba(200,100,0,0.1);border-radius: 5px;">'.$string.'</span>';


        return '<div>'.$string.'</div>';
    }

    public function setEditMode()
    {
        $value = 'N';

        if ( $_REQUEST['FILTER'] === 'on' )
        {
            $value = 'Y';
        }

        Option::set( 'dev.extended', 'drugs_edit_mode', $value);

        return $this->getEditMode();
    }

    public function getEditMode()
    {
        return Option::get( 'dev.extended', 'drugs_edit_mode') ? : 'N';
    }
}